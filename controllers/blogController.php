<?php 
//Fichero controllers/blogController.php

//El controlador llama al modelo de datos y pasa los resultados a la vista
require('models/postModel.php');
require('models/blogModel.php');

$blog=new Blog();

// Con el modelo de datos aquí, extraigo los datos que pasaré a la vista
if(isset($_GET['id'])){
	$entrada=$blog->dimeEntrada($_GET['id']);
	echo $twig->render('entrada.html.twig', Array('entrada'=>$entrada));
}else{
	$entradas=$blog->dimeEntradas();
	echo $twig->render('entradas.html.twig', Array('entradas'=>$entradas));
}

?>