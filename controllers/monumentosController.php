<?php  
//Fichero controllers/monumentosController.php


//El controlador, tiene que llamar al modelo
// de datos, y pasar los resultados a la vista
require('models/monumentoModel.php');
require('models/monumentosModel.php');
$monumentos=new Monumentos();

if(isset($_GET['id'])){
	//Para conseguir un único monumento
	$elmonumento=$monumentos->dimeElemento($_GET['id']);
	echo $twig->render('monumentoView.html.twig', Array('elmonumento'=>$elmonumento));
}else{
	//Para conseguir un listado de todos los monumentos
	$losmonumentos=$monumentos->dimeElementos();
	echo $twig->render('monumentosView.html.twig', Array('losmonumentos'=>$losmonumentos));
}
?>