<?php 
//Fichero controllers/productosController.php

//El controlador llama al modelo de datos y pasa los resultados a la vista
require('models/productoModel.php');
require('models/almacenModel.php');

$almacen=new Almacen();

// Con el modelo de datos aquí, extraigo los datos que pasaré a la vista
if(isset($_GET['idProd'])){
	$producto=$almacen->dimeProducto($_GET['idProd']);
	echo $twig->render('producto.html.twig', Array('producto'=>$producto));
}else{
	$productos=$almacen->dimeProductos();
	echo $twig->render('productos.html.twig', Array('productos'=>$productos));
}

?>