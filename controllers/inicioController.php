<?php 
//Fichero controllers/productosController.php

//El controlador llama al modelo de datos y pasa los resultados a la vista
require('models/productoModel.php');
require('models/almacenModel.php');
require('models/postModel.php');
require('models/blogModel.php');
require('models/centroModel.php');
require('models/centrosModel.php');
require('models/monumentoModel.php');
require('models/monumentosModel.php');

$almacen=new Almacen();
$blog=new Blog();
$centros=new Centros();
$monumentos=new Monumentos();

// Con el modelo de datos aquí, extraigo los datos que pasaré a la vista
	$productos=$almacen->dimeProductosTop();
	
	$entradas=$blog->dimeEntradasTop();

	$loscentros=$centros->dimeElementosTop();

	$losmonumentos=$monumentos->dimeElementosTop();
	echo $twig->render('inicio.html.twig', Array('productos'=>$productos, 'entradas'=>$entradas, 'loscentros'=>$loscentros, 'losmonumentos'=>$losmonumentos));


?>