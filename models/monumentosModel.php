<?php  
// Fichero models/monumentosModel.php
Class Monumentos{

	public $elementos;

	public function __construct(){
		$this->elementos=[];
	}

	public function dimeElementos(){
		$url='http://www.zaragoza.es/sede/servicio/monumento.json';
		$datos=file_get_contents($url);
		$info=json_decode($datos);
		foreach ($info->result as $elemento) {
			$this->elementos[]=new Monumento($elemento);
		}
		return $this->elementos;
	}

	public function dimeElementosTop(){
		$url='http://www.zaragoza.es/sede/servicio/monumento.json';
		$datos=file_get_contents($url);
		$info=json_decode($datos);
		$contador=0;
		foreach ($info->result as $elemento) {
			if($contador<=3){
				$this->elementos[]=new Monumento($elemento);
			}
			$contador++;
		}
		return $this->elementos;
	}

	public function dimeElemento($id){
		$url='http://www.zaragoza.es/sede/servicio/monumento.json';
		$datos=file_get_contents($url);
		$info=json_decode($datos);
		foreach ($info->result as $elemento) {
			$elemento=new Monumento($elemento);
			if($elemento->id==$id){
				return $elemento;
			}
		}	
	}

}