<?php 
//Fichero models/almacengModel.php

class Almacen{
	public $elementos; //Será un vector de elementos

	public function __construct(){
		$this->elementos=[]; // Le indico que será un vector vacio de momento
	}

	public function dimeProductos(){
		global $conexion; //Hago alusión a la conexión global
		$sql="SELECT *, (SELECT archivoImg FROM imagenes WHERE idProd=productos.idProd LIMIT 0,1) AS archivoImg FROM productos INNER JOIN  categorias ON productos.idCat=categorias.idCat  ORDER BY fechaAlta DESC";
		$consulta=$conexion->query($sql);
		while($registro=$consulta->fetch_array()){
			$this->elementos[]=new Producto($registro);
		}
		return $this->elementos; //Devuelve un array de elementos

	}

	public function dimeProducto($idProd){
		global $conexion; //Hago alusión a la conexión global
		$sql="SELECT *, (SELECT archivoImg FROM imagenes WHERE idProd=productos.idProd LIMIT 0,1) AS archivoImg FROM productos INNER JOIN  categorias ON productos.idCat=categorias.idCat WHERE idProd=$idProd";
		$consulta=$conexion->query($sql);
		$registro=$consulta->fetch_array();
		$elemento=new Producto($registro);
		return $elemento; //Devuelve un solo elemento

	}

	public function dimeProductosTop(){
		global $conexion; //Hago alusión a la conexión global
		$sql="SELECT *, (SELECT archivoImg FROM imagenes WHERE idProd=productos.idProd LIMIT 0,1) AS archivoImg FROM productos INNER JOIN  categorias ON productos.idCat=categorias.idCat  ORDER BY fechaAlta DESC LIMIT 0,3";
		$consulta=$conexion->query($sql);
		while($registro=$consulta->fetch_array()){
			$this->elementos[]=new Producto($registro);
		}
		return $this->elementos; //Devuelve un array de elementos

	}

} //Fin de la class Almacen

?>