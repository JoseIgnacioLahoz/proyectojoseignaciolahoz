<?php 
//Fichero models/productoModel.php

//Constructor de la clase Producto
class Producto{
	public $idProd;
	public $nombreProd;
	public $descripcionProd;
	public $precioProd;
	public $unidadesProd;
	public $fechaAlta;
	public $categoria;
	public $url;
	public $imagen;

	public function __construct($registro){
		$this->idProd=$registro['idProd'];
		$this->nombreProd=$registro['nombreProd'];
		$this->descripcionProd=$registro['descripcionProd'];
		$this->precioProd=$registro['precioProd'];
		$this->unidadesProd=$registro['unidadesProd'];
		$this->fechaAlta=$registro['fechaAlta'];
		$this->categoria=$registro['nombreCat'];
		$this->imagen=$registro['archivoImg'];

		$caracteres = array('Š'=>'S', 'š'=>'s', 'Đ'=>'Dj', 'đ'=>'dj', 'Ž'=>'Z', 'ž'=>'z', 'Č'=>'C', 'č'=>'c', 'Ć'=>'C', 'ć'=>'c', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E', 'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y', 'Ŕ'=>'R', 'ŕ'=>'r', ' '=>'-', ':'=>'');
		$nombreProd=strtr($this->nombreProd, $caracteres);
		$nombreProd=strtolower($nombreProd);

		$this->url='producto-'.$nombreProd.'-'.$this->idProd.'.html';
	}
} //Fin de la clase Producto

?>