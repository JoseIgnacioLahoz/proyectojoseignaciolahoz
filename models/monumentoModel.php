<?php  
// Fichero models/monumentoModel.php
Class Monumento{
	public $id;
	public $nombre;
	public $descripcion;
	public $estilo;
	public $direccion;
	public $datacion;
	public $longitud;
	public $latitud;

	public function __construct($elemento){
		$this->id=$elemento->id;
		$this->nombre=$elemento->title;
		$this->descripcion=$elemento->description;
		$this->estilo=$elemento->estilo;
		@$this->direccion=$elemento->address;
		@$this->datacion=$elemento->datacion;

		$utm = new UTMRef($elemento->geometry->coordinates[0], $elemento->geometry->coordinates[1], "T", 30);
    	$ll = $utm->toLatLng();

		@$this->longitud=$ll->lng;
		@$this->latitud=$ll->lat;
	}
}

?>